/*
  Name:    parkMonitoring.h
  Created: 16.02.2017
  Author:  Robert Mang
*/

#include <C:\workspace\parkMonitoring\src\UTFT\UTFT.cpp>
#include <C:\workspace\parkMonitoring\src\UTFT\UTFT.h>
#include <C:\workspace\parkMonitoring\src\UTFT\DefaultFonts.c>
#include <SoftwareSerial.h>
#include <avr\pgmspace.h>
#include "Wire.h"
#include "SPI.h"
#include "I2Cdev.h"
#include "MPU6050.h"

extern unsigned short car[];

UTFT myGLCD(SSD1289, 38, 39, 40, 41);

MPU6050 accelgyro;

// Error Message
const char errResp[] = "ERROR\r\n";
int errRespLen = strlen(errResp);

// DISPLAY
int picStartX = 80;
int picStartY = 80;
int picSizeX = 140;
int picSizeY = 72;

// GYRO
#define tresholdGyro 900

int16_t ax, ay, az;
int16_t gx, gy, gz;
int8_t threshold, count; 
float temp;
bool zero_detect; 
bool TurnOnZI = false;
bool XnegMD, XposMD, YnegMD, YposMD, ZnegMD, ZposMD;

// SIM
#define SIM800_TX_PIN     13
#define SIM800_RX_PIN     12
#define SIM800_NRES_PIN   18
#define STD_TIMEOUT       2 // seconds
SoftwareSerial simSerial(SIM800_TX_PIN, SIM800_RX_PIN);


// SONIC
#define BUF_SIZE 500
#define NUM_SENSORS 4
#define NUM_VALUES 5

int trigger[4] = { 9, 8, 11, 10 };  // VL, VR, HL, HR Pinkonfiguration beachten
int echo[4] = { 15, 14, 17, 16 };     // VL, VR, HL, HR Pinkonfiguration beachten

int displayXCoordinates[4] = {50, 50, 230, 230};// VR, VL, HR, HL
int displayYCoordinates[4] = {50, 175, 50, 175};// VR, VL, HR, HL

int distance[NUM_SENSORS][NUM_VALUES];

char valueArray[BUF_SIZE];

bool crashDetected;

/********** TIMINGS ************/
unsigned long timeSend;
unsigned long timeSonic;

bool readToggle;
bool sendingToggle;
#define SmsIntervall    30000
#define SonicIntervall  1000

// Funktionen
void printOnLCD(void);

void readSensorData(void);

void sendCrashNotification(void);
bool crashDetection(void);

String telegram;

int initWireless(void);
int setupWireless(void);
int initSIM(void);
int checkPin(void);
int simSerialInOut(const char *cmd, const char *expResp, unsigned int timeoutSec);
void serialDebug(void);
int checkIp(void);

void sendSmsTimer(void);
void readSonicTimer(void);
