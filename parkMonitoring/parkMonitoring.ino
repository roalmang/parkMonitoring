/*
  Name:    parkMonitoring.ino
  Created: 16.02.2017
  Author:  Robert Mang
*/

#include "parkMonitoring.h"

void setup() {
  Serial.begin(9600);
  Wire.begin();
  delay(1000);
  while (!Serial);

  randomSeed(analogRead(0));

  /* Disable unused serial interfaces */
  Serial2.end();
  Serial3.end();

  /*********** SONIC SETUP ***************/
  pinMode(trigger[0], OUTPUT);
  pinMode(trigger[1], OUTPUT);
  pinMode(trigger[2], OUTPUT);
  pinMode(trigger[3], OUTPUT);
  pinMode(echo[0], INPUT);
  pinMode(echo[1], INPUT);
  pinMode(echo[2], INPUT);
  pinMode(echo[3], INPUT);
  readSensorData();

  /*********** DISPLAY Setup ***************/
  myGLCD.InitLCD();
  myGLCD.setFont(BigFont);
  myGLCD.setBackColor(VGA_BLACK);
  myGLCD.fillScr(VGA_GREEN);
  myGLCD.drawBitmap(picStartX, picStartY, picSizeX, picSizeY, car);


  /*********** SIM/Wireles Setup ***************/
  simSerial.begin(9600);
  pinMode(SIM800_NRES_PIN, OUTPUT);
  digitalWrite(SIM800_NRES_PIN, HIGH);

  delay(1000);
  

  /*********** GYRO SETUP ***************/
  accelgyro.initialize();
  accelgyro.setAccelerometerPowerOnDelay(3);
  accelgyro.setIntZeroMotionEnabled(TurnOnZI);
  accelgyro.setDHPFMode(1);
  accelgyro.setMotionDetectionThreshold(2);
  accelgyro.setZeroMotionDetectionThreshold(2);
  accelgyro.setMotionDetectionDuration(40);
  accelgyro.setZeroMotionDetectionDuration(1);

  // 
  sendingToggle = true;
  readToggle = true;
  
  Serial.println("Setup Complete!");

}

void loop() {
  // Timer starten
  readSonicTimer();
  sendSmsTimer();
  
  // Sensoren auslesen
  crashDetected = crashDetection();
     
   if (readToggle) {
    readSensorData();
    printOnLCD();
    readToggle = false;
  }
  
  // Notification senden und Werte auf LCD ausgeben
  if (crashDetected && sendingToggle) {
    sendCrashNotification();
    Serial.println("Detected");
    Serial.print("gx: "); Serial.println(gx);
    Serial.print("gy: "); Serial.println(gy);
    Serial.print("gz: "); Serial.println(gy);
    Serial.print("Telegram: ");  Serial.println(telegram);
    crashDetected = false;
    sendingToggle = false;
  }
  
  return;
}


/************************ SONIC ************************************/
void readSensorData() {
  telegram = "";
  for (int j = 0; j < NUM_VALUES; j++) {
    for (int i = 0; i < NUM_SENSORS; i++) {
      int value = 0;
      int time = 0;
      digitalWrite(trigger[i], LOW); // Setze Spannung an Trigger auf 0 zur Rauschunterdrueckung
      delayMicroseconds(10);
      digitalWrite(trigger[i], HIGH); // Aktor starten, Ton fuer 10 ms
      delayMicroseconds(10);
      digitalWrite(trigger[i], LOW);

      
      time = pulseIn(echo[i], HIGH);
      
      // Kurzes Delay wegen Entprellung
      delayMicroseconds(10); 
            
      value = ((float)time * 0.03432) / 2.0;
      
      if (value >= 500) {
        value = 0;  // Abfangen von Messfehlern
      }
      distance[i][j] = value;
      telegram = telegram + value + ", ";
    }
    readToggle = false;
   }
   
}


/************************ DISPLAY ************************************/
void printOnLCD() {
  int avgValue[NUM_SENSORS] = {0, 0, 0, 0};

  myGLCD.clrScr();

  for (int j = 0; j < NUM_VALUES; j++) {
    for (int i = 0; i < NUM_SENSORS; i++)
      avgValue[i] = avgValue[i] + distance[i][j];
  }
  // car.c auf Display ausgeben
  myGLCD.drawBitmap(picStartX, picStartY, picSizeX, picSizeY, car);


  // Durchschnitt berechnen und auf Display ausgeben
  for (int i = 0; i < NUM_SENSORS; i++) {
    avgValue[i] = avgValue[i] / NUM_VALUES;
    myGLCD.printNumI(avgValue[i], displayXCoordinates[i], displayYCoordinates[i]);
  }
}


/************************ SIM ************************************/

/******************* NOTIFICATIONS ***********************/
void sendCrashNotification(void) {
  // Setze SMS-Format zu ASCII (Textformat)
  simSerial.write("AT+CMGF=1\r\n");
  delay(500);
  
  // SMS Kommando und Telefonnummer
  simSerial.write("AT+CMGS=\"017630143801\"\r\n");
  delay(500);
  
  // write()-Methode kommt nicht mit Strings klar, daher
  // Teleram-String zu Char-Array konvertieren
  int lengthTelegram = (int) telegram.length();
  const char telegramArray[lengthTelegram];
  telegram.toCharArray(telegramArray, lengthTelegram);

  simSerial.write(telegramArray);
  delay(500);

  //Send Ctrl+Z / ESC to denote SMS message is complete
  simSerial.write((char)26);
  
  Serial.println("Telegram Sent!");
}

bool crashDetection(void) {
  accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
  int gyroSum = abs(gx) + abs(gy) + abs(gz);
  if (gyroSum > tresholdGyro) {
    return true;
  }
  return false;
}

void readSonicTimer(void) {
  if (millis() - timeSonic > SonicIntervall) {
    timeSonic = millis();
    readToggle = true;
  }
}

void sendSmsTimer(void) {
  if (millis() - timeSend > SmsIntervall) {
    timeSend = millis();
    sendingToggle = true;
    // Debugausgabe
    Serial.println("Time is up for sending.");
  }
}

