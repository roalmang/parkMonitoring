\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Motivation}{3}{section.1}
\contentsline {section}{\numberline {2}System\IeC {\"u}bersicht}{4}{section.2}
\contentsline {section}{\numberline {3}Hardwarekomponenten}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Arduino}{5}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Ultraschallsensoren}{6}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}SIM800L GSM Modul}{7}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}3,2" TFT LCD Display}{8}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Gyroskop}{8}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}3D Drucker}{9}{subsection.3.6}
\contentsline {section}{\numberline {4}Geh\IeC {\"a}useaufbau}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Technische Zeichnungen}{10}{subsection.4.1}
\contentsline {section}{\numberline {5}Software}{12}{section.5}
\contentsline {subsection}{\numberline {5.1}Programmablaufplan}{12}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Arduino Programm}{13}{subsection.5.2}
\contentsline {section}{\numberline {6}Fazit}{21}{section.6}
\contentsline {subsection}{\numberline {6.1}Probleme}{21}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Ausblick}{21}{subsection.6.2}
