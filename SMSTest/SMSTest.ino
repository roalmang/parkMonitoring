#include <SoftwareSerial.h>
 
#define SIM800_TX_PIN 13
#define SIM800_RX_PIN 12
#define SIM800_NRES_PIN   18
 
//Create software serial object to communicate with SIM800
SoftwareSerial serialSIM800(SIM800_TX_PIN,SIM800_RX_PIN);

void setup() {
  //Begin serial comunication with Arduino and Arduino IDE (Serial Monitor)
  Serial.begin(9600);
  while(!Serial);
   
  //Being serial communication witj Arduino and SIM800
  serialSIM800.begin(9600);
  pinMode(SIM800_NRES_PIN, OUTPUT);
  digitalWrite(SIM800_NRES_PIN, HIGH);
  
  delay(1000);
   
  Serial.println("Setup Complete!");
  Serial.println("Sending SMS...");
   
  //Set SMS format to ASCII
  serialSIM800.write("AT+CMGF=1\r\n");
  delay(1000);
 
  //Send new SMS command and message number
  serialSIM800.write("AT+CMGS=\"017664396278\"\r\n");
  delay(1000);
  
  String message = "Diese SMS wird gerade von einem Bobbycar verschickt. Danke für deine Hilfe, funzt! - Robert";
  int lengthMessage = (int) message.length();
  const char messageArray[lengthMessage];
  message.toCharArray(messageArray, lengthMessage);
  //Send SMS content
  
  Serial.println(messageArray);
  serialSIM800.write(messageArray);
  delay(1000);
   
  //Send Ctrl+Z / ESC to denote SMS message is complete
  serialSIM800.write((char)26);
  delay(1000);
     
  Serial.println("SMS Sent!");
}
 
void loop() {
  
}
